package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/pierredittgen/gomoddep/domain"
)

var rootCmd *cobra.Command

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func Init(version string, repo domain.Repository) {
	rootCmd = &cobra.Command{
		Use:   "gomoddep",
		Short: "Gomoddep is a go.mod depency analyzer",
		Long: `An handy go.mod dependency analyzer built with
					  love by spf13 and friends in Go.
					  Complete documentation is available at http://gitlab.com/pierredittgen/gomoddep`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("gomoddep %s\n\nUse gomoddep --help for usage notice\n", version)
		},
	}

	var depCmd = &cobra.Command{
		Use:   "dep <dependency_name>",
		Short: "display go modules depending on given dependency",
		Long:  ``,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			depName := args[0]
			foundModules, err := repo.GetDependencies(depName, false)
			if err != nil {
				log.Fatal(err)
			}
			for _, mod := range foundModules {
				fmt.Println(mod)
			}
		},
	}

	var modCmd = &cobra.Command{
		Use:   "mod <module_name>",
		Short: "display dependencies of a go module",
		Long:  ``,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			modName := args[0]
			foundModules, err := repo.GetModules(modName)
			if err != nil {
				log.Fatal(err)
			}
			for _, mod := range foundModules {
				fmt.Println(mod)
			}
		},
	}

	rootCmd.AddCommand(depCmd)
	rootCmd.AddCommand(modCmd)
}
