package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "gomoddep",
	Short: "Gomoddep is a go.mod depency analyzer",
	Long: `An handy go.mod dependency analyzer
			built with love by spf13 and friends in Go.
			Complete documentation is available at http://gitlab.com/pierredittgen/gomoddep`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello there!")
	},
}
