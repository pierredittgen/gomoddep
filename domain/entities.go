package domain

import (
	"fmt"
	"strings"
)

type GoModule struct {
	FilePath string
	Path     string
	Requires []ModuleRequire
}

func (gmf GoModule) String() string {
	strRequires := make([]string, len(gmf.Requires))
	for i, req := range gmf.Requires {
		strRequires[i] = "  " + req.String()
	}
	return fmt.Sprintf("%s\n%s", gmf.Path, strings.Join(strRequires, "\n"))
}

type ModuleRequire struct {
	Path     string
	Version  string
	Indirect bool
}

func (r ModuleRequire) String() string {
	var indirectInfo string
	if r.Indirect {
		indirectInfo = " // indirect"
	}
	return fmt.Sprintf("%s@%s%s", r.Path, r.Version, indirectInfo)
}
