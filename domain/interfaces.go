package domain

type Service interface {
	BrowseModules(root string, excludedDirs []string) ([]GoModule, error)
}

type Source interface {
	ReadAll() ([]GoModule, error)
}

type Repository interface {
	GetModules(name string) ([]GoModule, error)
	GetDependencies(name string, indirectDependency bool) ([]GoModule, error)
}
