package main

import (
	_ "embed"
	"log"
	"os"

	"gitlab.com/pierredittgen/gomoddep/cmd"
	"gitlab.com/pierredittgen/gomoddep/repository"
	"gitlab.com/pierredittgen/gomoddep/source"
)

const ROOT_DIR_ENV_VAR = "ROOT_DIR"

//go:embed VERSION
var Version string

var EXCLUDE_DIRS = []string{".git", "node_modules"}

func main() {
	rootDir := os.Getenv(ROOT_DIR_ENV_VAR)
	if rootDir == "" {
		log.Fatalf("please define '%s' environment variable first", ROOT_DIR_ENV_VAR)
	}

	src := source.NewTreeSource(rootDir, EXCLUDE_DIRS)
	modules, err := src.ReadAll()
	if err != nil {
		log.Fatalf("an error occurred while reading modules: %s\n", err)
	}

	repo := repository.NewMemRepo(modules)

	cmd.Init(Version, repo)
	cmd.Execute()
}
