package parser

import (
	"io"
	"os"

	"gitlab.com/pierredittgen/gomoddep/domain"
	"golang.org/x/mod/modfile"
)

func ParseModFile(path string) (gmf domain.GoModule, err error) {
	fd, err := os.Open(path)
	if err != nil {
		return
	}
	content, err := io.ReadAll(fd)
	if err != nil {
		return
	}
	f, err := modfile.Parse(path, content, nil)
	if err != nil {
		return
	}
	gmf.FilePath = path
	gmf.Path = f.Module.Mod.Path
	for _, r := range f.Require {
		req := domain.ModuleRequire{
			Path:     r.Mod.Path,
			Version:  r.Mod.Version,
			Indirect: r.Indirect,
		}
		gmf.Requires = append(gmf.Requires, req)
	}
	return
}
