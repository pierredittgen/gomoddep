package fstree

import (
	"io/fs"
	"path/filepath"
)

// ExcludeDirFunc defines function receiving a directory name and saying if it's exluded or not
type ExcludeDirFunc func(string) bool

// ExcludeDirs generate a custom ExcludeDirFunc based on directory name list
func ExcludeDirs(dirs []string) ExcludeDirFunc {
	return func(name string) bool {
		for _, dir := range dirs {
			if name == dir {
				return true
			}
		}
		return false
	}
}

func AcceptAllDirs(name string) bool {
	return false
}

// FindFiles returns all files named as filename found in the given root directory and subdirectories
func FindFiles(root, filename string, excludeDirFunc func(string) bool) ([]string, error) {
	absolute_dir, err := filepath.Abs(root)
	if err != nil {
		return nil, err
	}

	files := []string{}
	walkFunc := func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			if excludeDirFunc(info.Name()) {
				return filepath.SkipDir
			}
		} else {
			if info.Name() == filename {
				files = append(files, path)

			}
		}
		return nil
	}

	err = filepath.Walk(absolute_dir, walkFunc)
	return files, err
}
