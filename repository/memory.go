package repository

import (
	"strings"

	"gitlab.com/pierredittgen/gomoddep/domain"
)

type MemRepository struct {
	modules []domain.GoModule
}

func NewMemRepo(modules []domain.GoModule) domain.Repository {
	return MemRepository{modules}
}

func (mr MemRepository) GetModules(name string) ([]domain.GoModule, error) {
	var mods []domain.GoModule
	for _, mod := range mr.modules {
		if strings.Contains(mod.Path, name) {
			mods = append(mods, mod)
		}
	}
	return mods, nil
}

func filterDependencies(mod domain.GoModule, name string, indirectDependency bool) []domain.ModuleRequire {
	var deps []domain.ModuleRequire
	for _, req := range mod.Requires {
		if strings.Contains(req.Path, name) && req.Indirect == indirectDependency {
			deps = append(deps, req)
		}
	}
	return deps
}

func (mr MemRepository) GetDependencies(name string, indirectDependency bool) ([]domain.GoModule, error) {
	var mods []domain.GoModule
	for _, mod := range mr.modules {
		deps := filterDependencies(mod, name, indirectDependency)
		if len(deps) != 0 {
			m := domain.GoModule{
				FilePath: mod.FilePath,
				Path:     mod.Path,
				Requires: deps,
			}
			mods = append(mods, m)
		}
	}
	return mods, nil
}
