package source

import (
	"log"

	"gitlab.com/pierredittgen/gomoddep/domain"
	"gitlab.com/pierredittgen/gomoddep/parser"
	"gitlab.com/pierredittgen/gomoddep/pkg/fstree"
)

const GOModuleFilename = "go.mod"

type TreeSource struct {
	Root         string
	ExcludedDirs []string
}

func NewTreeSource(root string, excludedDirs []string) domain.Source {
	return TreeSource{Root: root, ExcludedDirs: excludedDirs}
}

func (s TreeSource) ReadAll() ([]domain.GoModule, error) {
	// Gets go.mod files
	gomodfiles, err := fstree.FindFiles(s.Root, GOModuleFilename, fstree.ExcludeDirs(s.ExcludedDirs))
	if err != nil {
		return nil, err
	}

	// Convert them to gomod objects
	gomoditems := []domain.GoModule{}
	for _, path := range gomodfiles {

		gmf, err := parser.ParseModFile(path)
		if err != nil {
			log.Printf("error parsing %s: %s\n", path, err)
			continue
		}
		gomoditems = append(gomoditems, gmf)
	}

	return gomoditems, nil
}
